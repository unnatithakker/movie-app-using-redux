import {Cmd, loop} from 'redux-loop'
import {RECEIVE_CHART_DATA,REQUEST_CHART_DATA, REQUEST_SERIES_CHART_DATA, REQUEST_PIE_CHART_DATA} from '../actions/chart'
import {generateBarGraphData, generateSeriesGraphData, getPieCharts} from '../effects/chart'

export default (state = {config: {}, seriesConfig:{}, pieConfig: {}}, action) => {

	switch (action.type) {

		case REQUEST_CHART_DATA:
			return {
				...state,
				config : generateBarGraphData()
            }
        case REQUEST_SERIES_CHART_DATA:
			return {
				...state,
				seriesConfig : generateSeriesGraphData()
            }    
        case REQUEST_PIE_CHART_DATA:
			return {
				...state,
				pieConfig : getPieCharts()
            }    

		default:
			return state
	}
}