import {LOAD_FAV_MOVIE_DATA, REMOVE_FAV_MOVIE_DATA} from '../actions/favourites'
import {countFavouriteList} from '../utils/movies'
import {getFavourites, removeFavMovie} from '../reducers/movie'

export default (state = {name: null, data: [], favMovies:[]}, action) => {

	switch (action.type) {
        case LOAD_FAV_MOVIE_DATA:
			return{
				...state,
				data: state.data
			}	
		case REMOVE_FAV_MOVIE_DATA:
			return{
				...state,
				data: countFavouriteList(state, action.payload.data)
			}
		default:
			return state
    }
}