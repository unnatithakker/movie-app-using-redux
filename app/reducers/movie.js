import {Cmd, loop} from 'redux-loop'
import {REQUEST_MOVIE_DATA,receiveMovieData, RECEIVE_MOVIE_DATA, TOGGLE_FAVMOVIE_DATA, REMOVE_FAVMOVIE_DATA} from '../actions/movie'
import {fetchMovieData} from '../effects/movie'
import {toggleToFavouriteList, countFavouriteList} from '../utils/movies'

export default (state = {name: null, data: [], favMovies:[]}, action) => {

	switch (action.type) {

		case REQUEST_MOVIE_DATA:
			return loop(
				state,
				Cmd.run(fetchMovieData, {
					args                : [],
					successActionCreator: receiveMovieData,
					failActionCreator   : console.log
				})
			)

		case RECEIVE_MOVIE_DATA:
			return {
				...state,
				data: action.payload.data
			}

		case TOGGLE_FAVMOVIE_DATA:
			return {
				...state,
				favMovies: state.favMovies.concat(action.payload.data)
			}

		case REMOVE_FAVMOVIE_DATA:
			return {
				...state,
				favMovies: countFavouriteList(state.favMovies, action.payload.data)
			}
		default:
			return state
	}
}

export function getFavourites(state){
	return state;
}