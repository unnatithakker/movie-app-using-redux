import {combineReducers} from 'redux-loop'
import {routerReducer} from 'react-router-redux'

import sampleReducer from './sample.js'
import movieReducer from './movie.js'
import favReducer from './favourites.js'
import chartReducer from './chart.js'

const rootReducer = combineReducers({
	sample: sampleReducer,
	movie:movieReducer,
	fav:favReducer,
	router: routerReducer,
	chart: chartReducer
})

export default rootReducer
