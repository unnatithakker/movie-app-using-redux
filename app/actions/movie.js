export const REQUEST_MOVIE_DATA = "REQUEST_MOVIE_DATA"
export const RECEIVE_MOVIE_DATA = "RECEIVE_MOVIE_DATA"
export const TOGGLE_FAVMOVIE_DATA = "TOGGLE_FAVMOVIE_DATA"
export const REMOVE_FAVMOVIE_DATA = "REMOVE_FAVMOVIE_DATA"

export function requestMovieData() {
	return {
		type   : REQUEST_MOVIE_DATA,
		payload: {}
	}
}

export function receiveMovieData(data) {
	return {
		type   : RECEIVE_MOVIE_DATA,
		payload: {data}
	}
}

export function toggleToFavourite(data) {
	return {
		type   : TOGGLE_FAVMOVIE_DATA,
		payload: {data}
	}
}

export function removeFavourites(data) {
	return {
		type   : REMOVE_FAVMOVIE_DATA,
		payload: {data}
	}
}
