export const REQUEST_CHART_DATA = "REQUEST_CHART_DATA"
export const RECEIVE_CHART_DATA = "RECEIVE_CHART_DATA"
export const REQUEST_SERIES_CHART_DATA = "REQUEST_SERIES_CHART_DATA"
export const REQUEST_PIE_CHART_DATA = "REQUEST_PIE_CHART_DATA"

export function requestChartData() {
	return {
		type   : REQUEST_CHART_DATA,
		payload: {}
	}
}

export function receiveChartData(data) {
	return {
		type   : RECEIVE_CHART_DATA,
		payload: {data}
	}
}
export function requestSeriesChartData() {
	return {
		type   : REQUEST_SERIES_CHART_DATA,
		payload: {}
	}
}
export function requestPieChartData() {
	return {
		type   : REQUEST_PIE_CHART_DATA,
		payload: {}
	}
}