export const LOAD_FAV_MOVIE_DATA = "LOAD_FAV_MOVIE_DATA"
export const REMOVE_FAV_MOVIE_DATA = "REMOVE_FAV_MOVIE_DATA"

export function loadFavMovieData() {
	return {
		type   : LOAD_FAV_MOVIE_DATA,
		payload: {}
	}
}
export function removeFavourites(data) {
	return {
		type   : REMOVE_FAV_MOVIE_DATA,
		payload: {data}
	}
}