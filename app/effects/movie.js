export const fetchMovieData = () => {
	return window.fetch('https://thoughtworksreactreduxmovies.firebaseio.com/movies.json')
    .then((data) => data.json())
	.catch(console.log)
}
