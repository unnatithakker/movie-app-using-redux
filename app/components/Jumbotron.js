import React from 'react'

export default class Jumbotron extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
			<section className="jumbotron text-center">
				<div className="container titleMovie">
					<h1 className="movieHeader">{this.props.name}</h1>
				</div>
			</section>
		)
	}
}
