import React from 'react'

export default class MovieItem extends React.Component {
	constructor(props) {
        super(props);
        //  this.state={
        //      favouriteCount : 0
        //  }
         this.addToFavourites = this.addToFavourites.bind(this);
         this.removeFromFavourites = this.removeFromFavourites.bind(this);
    }

    render() {
        const movie = this.props.movie;
       console.log(this.props.isFavourite)
		return (
            
            <div className="col col-4" key={movie.imdbID} >
                    <div className="card">
                        <img className="movie card-img-top" src={movie.Poster} />
                        <div className="card-body"><div className="col col-12">
                            <a className="movie-title card-title text-truncate" href="">{movie.Title}</a>
                            <div className="movie-year">{movie.Year}</div>
                            <div><button className="btn btn-secondary m-2">{movie.Type}</button>
                            
                            {!this.props.isFavourite ? (
                                <button id={`fav${movie._id}`} className="btn btn-info m-2" onClick={this.addToFavourites}>Add To Favourites</button>
                             ) : (
                                  <button id={`fav${movie._id}`} className="btn btn-info m-2" onClick={this.removeFromFavourites}>Remove from Favourite</button>
                                
                             )}
                            
                            </div>
                        </div></div>
                    </div>
            </div>
        )
    }

    addToFavourites(){
        console.log(this.props.movie)
        this.props.movie['isFavourite'] = this.props.movie['isFavourite'] ? false : true;
        this.props.onMarkAsFav(this.props.movie);
    }

    removeFromFavourites(){
        this.props.movie['isFavourite'] = this.props.movie['isFavourite'] ? false : true;
        this.props.onRemoveAsFav(this.props.movie);
    }

    componentDidMount(){
        console.log('mouted..')
    }
}