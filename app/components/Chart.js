import React from 'react'
import {Link} from 'react-router-dom'
//import FavouriteList from '../containers/favourite-data-list'
const ReactHighcharts = require('react-highcharts');

export default class Chart extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
            <div>
			<ReactHighcharts config = {this.props.config} ></ReactHighcharts>

            <ReactHighcharts config = {this.props.seriesData} ></ReactHighcharts>

            <ReactHighcharts config = {this.props.pieData} ></ReactHighcharts>
            </div>
		)
    }
    
    componentWillMount(){
         this.props.requestChartData();
         this.props.requestSeriesChartData();
         this.props.requestPieChartData();
    }
}
