import React from 'react'
import {Link} from 'react-router-dom'


export default class Header extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
			<header className="navbar navbar-dark bg-dark box-shadow fixed-top">
				<div className="container d-flex justify-content-between">
					<Link to="/" className="navbar-brand d-flex align-items-center">
						<strong>Sample App</strong>
					</Link>
					<Link to="/favouriteList" className="gray d-flex align-items-center">
						Favourite List
					</Link>
					<Link to="/charts" className="gray d-flex align-items-center">
						Charts
					</Link>
				</div>
			</header>
		)
	}
}
