import React from 'react'
import MovieItem from './MovieItem'


export default class FavouriteList extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		const movieList = this.props.data;
		const isFavourite = true;
		console.log(this.props)
		return (
			<div className="album py-5">
				<div className="container-fluid">
					<div className="row">
						{!movieList.length ? ( 'No Favourites Found') :
						(movieList.map(movie => <MovieItem key={movie.imdbID} isFavourite={isFavourite} movie={movie} onRemoveAsFav={this.props.removeFavourites} />))}
					</div>
				</div>
			</div>
		)
	}
	 componentWillMount(){
		 this.props.loadFavMovieData()
		  console.log('will mount')
		 
    }
}
