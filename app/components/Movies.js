import React from 'react'
import MovieItem from './MovieItem'


export default class Movies extends React.Component {
	constructor(props) {
        super(props)
	}

	render() {
        const movieList = this.props.data;
        const favMovies = this.props.favMovies;
        console.log(favMovies)
		return (
			<div>
                <div className="row"><div className="col-9">Movies </div><div className="col-3 text-center">Favourites: ({favMovies.length})</div></div>
                <div className="row mt-2" >
                    {movieList.map(movie => {
                    let isFavourite = false;
                    if(favMovies.length)
                        isFavourite = favMovies.filter(favMovie => favMovie.imdbID == movie.imdbID)
                        isFavourite = isFavourite.length ? true : false
                    return <MovieItem key={movie.imdbID} movie={movie} isFavourite={isFavourite} onMarkAsFav={this.props.toggleToFavourite} onRemoveAsFav={this.props.removeFavourites} />
                    } )}
                </div>
            </div>
		)
    }

    componentWillMount(){
         this.props.requestMovieData();
    }
}