import React from 'react'
import {Route} from 'react-router-dom'
import SamplePage from './Sample'
import Header from '../components/Header'
import FavouriteList from './FavouriteList'
import Charts from './Charts'

const App = () =>
	<div>
		<Header/>
		<Route exact path="/" component={SamplePage}/>
		<Route exact path="/favouriteList" component={FavouriteList}/>
		<Route exact path="/charts" component={Charts}/>
	</div>


export default App
