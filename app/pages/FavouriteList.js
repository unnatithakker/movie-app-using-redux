import React from 'react'
import {Link} from 'react-router-dom'
import FavouriteList from '../containers/favourite-data-list'

const Favourites = () =>
	<FavouriteList />

export default Favourites
