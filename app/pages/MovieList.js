import React from 'react'
import Jumbotron from '../containers/jumbotron'
import Movies from '../containers/movie-data-list'

const SamplePage = () =>
	<div className="sample-page">
		<Jumbotron/>
		<Movies/>
	</div>

export default SamplePage
