import React from 'react'
import {connect} from "react-redux"
import {requestChartData, requestSeriesChartData, requestPieChartData} from '../actions/chart'
import Chart from '../components/Chart'


export const mapStateToProps = (state) => {
	return {
		config: state.chart.config,
		seriesData: state.chart.seriesConfig,
		pieData: state.chart.pieConfig
	}
}

export const mapDispatchToProps = (dispatch) => {
	return {
		requestChartData : () => dispatch(requestChartData()),
		requestSeriesChartData : () => dispatch(requestSeriesChartData()),
		requestPieChartData : () => dispatch(requestPieChartData())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Chart)
