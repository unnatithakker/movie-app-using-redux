import React from 'react'
import {connect} from "react-redux"
import FavouriteList from '../components/FavouriteList'
import {loadFavMovieData, removeFavourites} from '../actions/favourites'
import {getFavourites, removeFavMovie} from '../reducers/movie'

// This is a container

export const mapStateToProps = (state) => {
	return {
		data: getFavourites(state).movie.favMovies,
		favMovies : state.fav.data
	}
}
export const mapDispatchToProps = (dispatch) => {
	return {
		loadFavMovieData: () => dispatch(loadFavMovieData()),
		removeFavourites: (movie) => dispatch(removeFavourites(movie))
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(FavouriteList)
