import React from 'react'
import {connect} from "react-redux"
import {requestMovieData, toggleToFavourite, removeFavourites} from '../actions/movie'
import Movies from '../components/Movies'

// This is a container

export const mapStateToProps = (state) => {
	return {
        data: state.movie.data,
        favMovies: state.movie.favMovies
	}
}
export const mapDispatchToProps = (dispatch) => {
	return {
        toggleToFavourite: (movie) => dispatch(toggleToFavourite(movie)),
        requestMovieData: () => dispatch(requestMovieData()),
        removeFavourites: (movie) => dispatch(removeFavourites(movie))
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(Movies)
